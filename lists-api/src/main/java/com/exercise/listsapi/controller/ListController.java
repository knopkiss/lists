package com.exercise.listsapi.controller;

import com.exercise.listsengine.ListsManipulator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@RestController
@RequestMapping("api")
public class ListController {

    private ListsManipulator listsManipulator;

    @Autowired
    public ListController(ListsManipulator listsManipulator) {
        this.listsManipulator = listsManipulator;
    }

    @PostMapping("flatten/sorted")
    public ResponseEntity<List<Integer>> getFlattenSorted(@RequestBody List<List<Integer>> listOfIntegerLists) {

        throw new UnsupportedOperationException();
        //return ResponseEntity.ok(listsManipulator.sortLists(listOfIntegerLists));
    }
}

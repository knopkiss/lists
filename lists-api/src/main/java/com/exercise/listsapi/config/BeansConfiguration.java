package com.exercise.listsapi.config;

import com.exercise.listsengine.ListsManipulator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansConfiguration {
    @Bean
    ListsManipulator getListsManipulator() {
        return new ListsManipulator();
    }
}
